import User from '../interfaces/interface';
import mongoose,{Schema} from 'mongoose';

const userschema:Schema=new Schema({
	Name:{type:String,required:true},
	Place:{type:String,required:true},
	Email:{type:String,required:true},
	Age:{type:Number,required:true}
	},
	{timestamps:true}
	);

export default mongoose.model<User>('USER',userschema);