import joi from 'joi';

// Schema_function create user
const validateCreateUser=function validate(user:any)
{
	const joischema=joi.object({
		Name:joi.string().required(),
		Place:joi.string().required(),
	    Mail:joi.string().required(),
	}).options({abortEarly:false});
	return joischema.validate(user);
}

// Schema_function update Email
const validateUpdateMail=function validate(user:any)
{
	const joischema=joi.object({
		Name:joi.string().required(),
		Mail:joi.string().required()
	}).options({abortEarly:false});
	return joischema.validate(user);
}


export default {validateCreateUser,validateUpdateMail}
