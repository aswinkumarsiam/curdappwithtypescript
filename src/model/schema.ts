
import mongoose,{Schema} from "mongoose"
import user from "./interface/user_interface"
const user_schema:Schema=new Schema({
    Name:{
        type:String,
        required:true
    },
    Place:{
        type:String,
        required:true
    },
    Mail:{
        type:String,
        required:true

    }

})
export default mongoose.model<user>("user",user_schema)
