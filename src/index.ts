import express from 'express';
import {json} from 'body-parser';
import  mongoose from 'mongoose';
import {apiroute} from "./routes/route";

const app= express();
app.use(json());
app.use(apiroute);

mongoose.connect("mongodb://localhost:27017/ts_app",()=>{
	console.log("MongoDb Connected")
});
app.listen(3001,()=>{
	console.log("Server Running..!");
})
