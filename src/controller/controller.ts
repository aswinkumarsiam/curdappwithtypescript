

import {Request,Response} from 'express';
import mongoose from 'mongoose';
import model from '../model/schema';
import validator from '../validater/validation';

//controller for get all user

const getuser=(req:Request,res:Response)=>{

	model.find()
	.then(result=>{
		res.status(200).json({status:"true",User:result,UserCount:result.length})
	})
	.catch(error=>{
		res.status(400).json({status:"false",Error:error})
	})


}
// controller create a new user

const createUser=(req:Request,res:Response)=>
{
	let {Name,Place,Mail}=req.body;
	console.log(req.body.Name)
	console.log(req.body.Mail)

	let response=validator.validateCreateUser(req.body);

	if(response.error)
	{
		res.status(400).json({"Error":response.error.details[0].message})

	}
	else
	{
		const user=new model({_id:new mongoose.Types.ObjectId(),Name,Place,Mail});
		user.save()
		.then(result=>{
		res.status(200).json({status:"true",Message:"User Created.."})
		})
		.catch(error=>{
		res.status(400).json({status:"false",Error:error})	
		})

	}
	
}

// controller for get particular user by name

const getUserbyName=(req:Request,res:Response)=>
{
	model.find({Name:req.query.Name})
	.then(result=>{
		res.status(200).json({status:"true",User:result})
	})
	.catch(error=>{
	res.status(400).json({status:"false",Error:error})		
	})
}

// controller for update Email of particular user 
const updateMail=(req:Request,res:Response)=>{

	var user={Name:req.query.Name,Mail:req.body.Mail};
	let response=validator.validateUpdateMail(user);
	if(response.error)
	{
		res.status(400).json({"Error":response.error.details[0].message})

	}
	else
	{
		
		model.findOneAndUpdate({Name:req.query.name},{Email:req.body.Email})
		.then(result=>{
		res.status(200).json({status:"true","Message":"Email Updated.."})
		})
		.catch(error=>{
		res.status(400).json({status:"false",Error:error})			
		})
	}

}
// controller for delete a particular user..
const deleteUser=(req:Request,res:Response)=>{
	model.findOneAndRemove({Name:req.query.name})
	.then(result=>{
		res.status(200).json({status:"true","Message":"User Deleted.."})
	})
	.catch(error=>{
	res.status(400).json({status:"false",Error:error})				
	})
}
export default {getuser,createUser,getUserbyName,updateMail,deleteUser}
