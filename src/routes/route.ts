import express from 'express';
import apicontroller from '../controller/controller';

const router=express.Router();
//URL for Get All User
router.get("/getuser",apicontroller.getuser);
//Url For create User
router.post("/createuser",apicontroller.createUser);
// Url for Get User by Name..
router.get("/getuserbyname",apicontroller.getUserbyName);
//Url For Update Email Of Particular user by name..
router.put("/updatemail",apicontroller.updateMail);
//Url for Delete User
router.delete("/deleteuser",apicontroller.deleteUser);

export {router as apiroute}
